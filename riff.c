#include <stdio.h>
#include <string.h>

struct RIFF{
	unsigned char HDR[4];
	int size;
	unsigned char RIFF_TYPE[4];
} __attribute__((packed));

struct FMT
{
	unsigned char chunk_id[4];
	int chunk_size;
	unsigned short compression;
	unsigned short chan_num;
	int sample_rate;
	int avg_bpm;
	short align;
	short sign_bpm;
	// short extra;
} __attribute__((packed));;

struct DATA {
	unsigned char chunk_id[4];
	int chunk_size;
};

struct LIST
{
	unsigned char chunk_id[4];
	int chunk_size;
};

struct INFO
{
	unsigned char chunk_id[4];
	int chunk_size;	
	unsigned char chunk_info[4];
};

enum FIELD_ENUM  {
	IALB = 0x424C4149,
	IARL = 0x4C524149,
	IART = 0x54524149,
	ICMS = 0x534D4349,
	ICMT = 0x544D4349,
	ICOP = 0x504F4349,
	ICRD = 0x44524349,
	ICRP = 0x50524349,
	IDIM = 0x4D494449,
	IDPI = 0x49504449,
	IENG = 0x474E4549,
	IGNR = 0x524E4749,
	IKEY = 0x59454B49,
	ILGT = 0x54474C49,
	IMED = 0x44454D49,
	INAM = 0x4D414E49,
	IPLT = 0x544C5049,
	IPRD = 0x44525049,
	ISBJ = 0x4A425349,
	ISFT = 0x54465349,
	ISHP = 0x50485349,
	ISRC = 0x43525349,
	ISRF = 0x46525349,
	ITCH = 0x48435449
};

struct INFO_FIELD
{
	FIELD_ENUM chunk_id;
	int chunk_size;	
};

struct HEADER_LIST
{
	FIELD_ENUM field;
	char* value;
	HEADER_LIST* next;
};

const char* value = "I'm a technick";

int main()
{
	RIFF *riff = new RIFF;
	FMT  *fmt = new FMT;
	DATA *data = new DATA;
	INFO *info = new INFO;
	INFO_FIELD *field = new INFO_FIELD;
	HEADER_LIST *headers = NULL,
		*current_headr = NULL;
	char field_name[5] = {0};
	char field_name2[5] = {0};
	char buffer[1024];
	char *wavdata = NULL;
	int  info_size = 0;
	FILE* fd = fopen("./test.wav", "rb");
	FILE* out = fopen("./out.wav", "wb");
	fread(riff, sizeof(RIFF), 1, fd);
	fread(fmt, sizeof(FMT), 1, fd);
	fread(data, sizeof(DATA), 1, fd);

	strncpy(field_name, (const char*)riff->HDR, 4);
	strncpy(field_name2, (const char*)riff->RIFF_TYPE, 4);
	printf(
		"File type: %s\n"
		"File size: %d\n"
		"Content:   %s\n",
		field_name, riff->size, field_name2);

	strncpy(field_name, (const char*)fmt->chunk_id, 4);
	printf(
		"Chunk name:  %s\n"
		"Chunk size:  %d\n"
		"compression: %d\n"
		"Channels #:  %d\n"
		"sample_rate: %d\n"
		"Avg bitrate: %d\n",
		field_name,
		fmt->chunk_size,
		fmt->compression,
		fmt->chan_num,
		fmt->sample_rate,
		fmt->avg_bpm
	);

	printf("Audio data size: %d\n", data->chunk_size);
	wavdata = new char[data->chunk_size];
	fread(wavdata, sizeof(char), data->chunk_size, fd);
	if (!feof(fd)) {
		fread(info, sizeof(INFO), 1, fd);
		printf("Size INFO list size: %d\n\n", info->chunk_size);
		while(!feof(fd)) {
			if (1 == fread(field, sizeof(INFO_FIELD), 1, fd)) {
				memset(buffer, 0, sizeof(buffer));
				strncpy(field_name, (const char*)&field->chunk_id, 4);
				fread(buffer, sizeof(char), field->chunk_size, fd);
				printf("%s = %s\n", field_name, buffer);
				if (headers) {
					headers->next = new HEADER_LIST;
					current_headr = headers->next;
				} else {
					headers = new HEADER_LIST;
					current_headr = headers;
				}
				current_headr->next = NULL;
				current_headr->field = field->chunk_id;
				current_headr->value = new char[strlen(field_name) + 1];
				strcpy(current_headr->value, field_name);
			} else {
				break;
			}
		}
	} else {
		delete info;
		info = NULL;
		printf("End of file\n");
	}

	// int new_size = riff->size;
	// fseek(out, 4, new_size);
	if (out) {
		fwrite(riff, sizeof(RIFF), 1, out);
		fwrite(fmt, sizeof(FMT), 1, out);
		fwrite(data, sizeof(DATA), 1, out);
		fwrite(wavdata, sizeof(char), data->chunk_size, out);
		if (!info) {
			info = new INFO;
		}
		current_headr = headers;
		while(current_headr) {
			if (current_headr->field == IALB) {
				current_headr->value = new char[strlen(value) + 1];
				strcpy(current_headr->value, value);
			}
			info_size += sizeof(HEADER_LIST) + strlen(current_headr->value) + 1;
			current_headr = current_headr->next;
			;
		}
		if (!headers) {
			headers = new HEADER_LIST;
			headers->field = IALB;
			headers->value = new char[strlen(value) + 1];
			strcpy(headers->value, value);
		}
		;
		if (!info) {
			info = new INFO;
			info->chunk_size = info_size;
			strcpy((char *)info->chunk_id, "LIST");
			strcpy((char *)info->chunk_info, "INFO");
		}
		fwrite(info, sizeof(INFO), data->chunk_size, out);
	}

	fclose(fd);
	fclose(out);
}